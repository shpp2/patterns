package shpp.com.level4.services;

import shpp.com.level4.model.channel.*;

public class ChannelFactory implements Factory {
    @Override
    public Channel create(ChannelType type) {
        switch (type) {
            case SINCE:
                return new SinceChannel();
            case NEWS:
                return new NewsChannel();
            case MUSIC:
                return new MusicChannel();
            case WEATHER:
                return new WeatherChannel();
            default:
                return null;
        }
    }
}