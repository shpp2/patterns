package shpp.com.level4.services;

import shpp.com.level4.model.channel.ChannelType;
import shpp.com.level4.model.channel.Channel;

public interface Factory {
    Channel create(ChannelType type);
}
