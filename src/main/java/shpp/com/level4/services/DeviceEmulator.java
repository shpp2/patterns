package shpp.com.level4.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import shpp.com.level4.model.channel.*;
import shpp.com.level4.model.devices.Device;

public class DeviceEmulator {
    private Channel channel;
    private final Logger logger = LoggerFactory.getLogger(DeviceEmulator.class);

    public DeviceEmulator(Device device) {
        logger.info("Devise is: {}", device.getStatus());
        this.channel = device.getChannel();
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    public void switchChannel() {
        if (channel instanceof SinceChannel) {
            setChannel(new NewsChannel());
        } else if (channel instanceof NewsChannel) {
            setChannel(new MusicChannel());
        } else if (channel instanceof MusicChannel) {
            setChannel(new WeatherChannel());
        } else if (channel instanceof WeatherChannel) {
            setChannel(new SinceChannel());
        }
    }

    public void playChannel() {
        channel.play();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            logger.warn("Interrupted!", e);
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
    }
}
