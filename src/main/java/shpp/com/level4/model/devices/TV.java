package shpp.com.level4.model.devices;

import shpp.com.level4.model.channel.Channel;
import shpp.com.level4.model.channel.ChannelType;
import shpp.com.level4.services.ChannelFactory;

public class TV implements Device {
    private Channel channel = new ChannelFactory().create(ChannelType.SINCE);
    private static final String STATUS = "TV";

    @Override
    public Channel getChannel() {
        return this.channel;
    }

    @Override
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public String getStatus() {
        return STATUS;
    }
}
