package shpp.com.level4.model.devices;

import shpp.com.level4.model.channel.Channel;

public interface Device {
    Channel getChannel();
    void setChannel(Channel channel);
    String getStatus();
}
