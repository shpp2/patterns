package shpp.com.level4.model.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SinceChannel implements Channel {
    private final Logger logger = LoggerFactory.getLogger(SinceChannel.class);

    @Override
    public void play() {
        logger.info(" Play since channel ! ");
    }
}
