package shpp.com.level4.model.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewsChannel implements Channel {
    private final Logger logger = LoggerFactory.getLogger(NewsChannel.class);

    @Override
    public void play() {
        logger.info(" Play news channel ! ");
    }
}
