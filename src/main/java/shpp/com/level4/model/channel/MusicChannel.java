package shpp.com.level4.model.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MusicChannel implements Channel {
    private final Logger logger = LoggerFactory.getLogger(MusicChannel.class);

    @Override
    public void play() {
        logger.info(" Play music channel ! ");
    }
}
