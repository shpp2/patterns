package shpp.com.level4.model.channel;

public enum ChannelType {
    SINCE,
    NEWS,
    MUSIC,
    WEATHER
}
