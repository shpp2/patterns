package shpp.com.level4.model.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeatherChannel implements Channel {
    private final Logger logger = LoggerFactory.getLogger(WeatherChannel.class);

    @Override
    public void play() {
        logger.info(" Play weather channel ! ");
    }
}
