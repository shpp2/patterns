package shpp.com.level4.model.channel;

public interface Channel {
    void play();
}
