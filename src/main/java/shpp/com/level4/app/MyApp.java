package shpp.com.level4.app;

import shpp.com.level4.model.devices.Device;
import shpp.com.level4.model.devices.Radio;
import shpp.com.level4.model.devices.TV;
import shpp.com.level4.services.DeviceEmulator;
import shpp.com.level4.util.PropertiesLoader;

public class MyApp {
    public static void main(String[] args) {
        Device tv = new TV();
        Device radio = new Radio();
        String propertiesValue = new PropertiesLoader().loadProperties().getProperty("numberOfIteration");
        int numberOfIteration = Integer.parseInt(propertiesValue);
        testDeviceEmulator(tv, numberOfIteration);
        testDeviceEmulator(radio, numberOfIteration);
    }

    private static void testDeviceEmulator(Device device, int numberOfIteration) {
        DeviceEmulator emulator = new DeviceEmulator(device);
        for (int i = 0; i < numberOfIteration; i++) {
            emulator.playChannel();
            emulator.switchChannel();
        }
    }
}



