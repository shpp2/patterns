package shpp.com.level4.model.channel;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;

class NewsChannelTest {

    @Test
    void playTest() {
        NewsChannel channel = Mockito.mock(NewsChannel.class);
        Mockito.doNothing().when(channel).play();
        channel.play();
        Mockito.verify(channel, times(1)).play();
    }
}