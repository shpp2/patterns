package shpp.com.level4.model.channel;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;

class SinceChannelTest {

    @Test
    void playTest() {
        SinceChannel channel = Mockito.mock(SinceChannel.class);
        Mockito.doNothing().when(channel).play();
        channel.play();
        Mockito.verify(channel, times(1)).play();
    }
}