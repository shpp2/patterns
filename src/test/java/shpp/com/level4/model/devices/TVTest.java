package shpp.com.level4.model.devices;

import org.junit.jupiter.api.Test;
import shpp.com.level4.model.channel.Channel;
import shpp.com.level4.model.channel.ChannelType;
import shpp.com.level4.services.ChannelFactory;

import static org.junit.jupiter.api.Assertions.*;

class TVTest {

    @Test
    void getChannelTest() {
        TV tv = new TV();
        Channel actual = tv.getChannel();
        assertNotNull(actual);
    }

    @Test
    void setChannelTest() {
        TV tv = new TV();
        Channel expected = new ChannelFactory().create(ChannelType.MUSIC);
        tv.setChannel(expected);
        Channel actual = tv.getChannel();
        assertEquals(expected, actual);
    }

    @Test
    void getStatusTest() {
        TV tv = new TV();
        String expected = "TV";
        String actual = tv.getStatus();
        assertEquals(expected, actual);
    }
}