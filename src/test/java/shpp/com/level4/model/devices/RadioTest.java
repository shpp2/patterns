package shpp.com.level4.model.devices;

import org.junit.jupiter.api.Test;
import shpp.com.level4.model.channel.Channel;
import shpp.com.level4.model.channel.ChannelType;
import shpp.com.level4.services.ChannelFactory;

import static org.junit.jupiter.api.Assertions.*;

class RadioTest {

    @Test
    void getChannelTest() {
        Radio radio = new Radio();
        Channel actual = radio.getChannel();
        assertNotNull(actual);
    }

    @Test
    void setChannelTest() {
        Radio radio = new Radio();
        Channel expected = new ChannelFactory().create(ChannelType.MUSIC);
        radio.setChannel(expected);
        Channel actual = radio.getChannel();
        assertEquals(expected, actual);
    }

    @Test
    void getStatusTest() {
        Radio radio = new Radio();
        String expected = "Radio";
        String actual = radio.getStatus();
        assertEquals(expected, actual);
    }
}