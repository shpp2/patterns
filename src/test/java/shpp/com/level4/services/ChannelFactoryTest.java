package shpp.com.level4.services;

import org.junit.jupiter.api.Test;
import shpp.com.level4.model.channel.Channel;
import shpp.com.level4.model.channel.ChannelType;

import static org.junit.jupiter.api.Assertions.*;

class ChannelFactoryTest {

    @Test
    void createChannelMusic() {
        Channel expected = new ChannelFactory().create(ChannelType.MUSIC);
        Channel actual = new ChannelFactory().create(ChannelType.MUSIC);
        assertNotEquals(expected, actual);
    }
}