package shpp.com.level4.services;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shpp.com.level4.model.channel.Channel;
import shpp.com.level4.model.channel.MusicChannel;
import shpp.com.level4.model.channel.NewsChannel;
import shpp.com.level4.model.channel.WeatherChannel;
import shpp.com.level4.model.devices.Device;
import shpp.com.level4.model.devices.Radio;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class DeviceEmulatorTest {

    @Test
    void setChannelTest() {
        Device radio = new Radio();
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel expected = new NewsChannel();
        emulator.setChannel(expected);
        Channel actual = emulator.getChannel();
        assertEquals(expected, actual);
    }

    @Test
    void switchChannelTest() {
        Device radio = new Radio();
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel expected = emulator.getChannel();
        emulator.switchChannel();
        Channel actual = emulator.getChannel();
        assertNotEquals(expected, actual);
    }

    @Test
    void switchChannelTestSwitchFromNewsChannel() {
        Device radio = new Radio();
        radio.setChannel(new NewsChannel());
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel expected = emulator.getChannel();
        emulator.switchChannel();
        Channel actual = emulator.getChannel();
        assertNotEquals(expected, actual);
    }

    @Test
    void switchChannelTestSwitchFromMusicChannel() {
        Device radio = new Radio();
        radio.setChannel(new MusicChannel());
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel expected = emulator.getChannel();
        emulator.switchChannel();
        Channel actual = emulator.getChannel();
        assertNotEquals(expected, actual);
    }

    @Test
    void switchChannelTestSwitchFromWeatherChannel() {
        Device radio = new Radio();
        radio.setChannel(new WeatherChannel());
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel expected = emulator.getChannel();
        emulator.switchChannel();
        Channel actual = emulator.getChannel();
        assertNotEquals(expected, actual);
    }

    @Test
    void playChannelTest() {
        DeviceEmulator emulator = Mockito.mock(DeviceEmulator.class);
        Mockito.doNothing().when(emulator).playChannel();
        emulator.playChannel();
        Mockito.verify(emulator, times(1)).playChannel();
    }

    @Test
    void getChannel() {
        Device radio = new Radio();
        Channel expected = radio.getChannel();
        DeviceEmulator emulator = new DeviceEmulator(radio);
        Channel actual = emulator.getChannel();
        assertEquals(expected, actual);
    }
}