package shpp.com.level4.util;

import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesLoaderTest {

    @Test
    void loadPropertiesLoadAllProperty() {
        PropertiesLoader loader = new PropertiesLoader();
        Properties properties = loader.loadProperties();
        int actual = properties.size();
        int expected = 3;
        assertEquals(expected, actual);
    }
}